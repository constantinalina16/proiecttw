Am creat baza de date ManagerRestaurant care contine tabelele Clients, Addresses, Orders, Products si Deliveries.
Un client poate sa faca o comanda specificand produsul sau produsele dorite si anumite date de contact precum numele, prenumele si adresa, dar si modalitatea de plata. Astfel toate datele furnizate de client sunt stocate in baza de date.

Se pot face operatii de post, get, put si delete.
Un client poate sa efectueze o comanda prin operatia de post. Clientii ce folosesc aplicatia pot fi previzualizati prin metoda get, iar statusul comenzilor(livrata/in procesare) poate fi schimbat prin metoda put. De asemenea o comanda poate fi stearsa folosind metoda delete.


